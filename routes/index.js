var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var exec = require('child_process').exec;
var cmd = 'nightwatch tests/test.js';
var str = '';
var async = require("async");

router.get('/', function(req, res, next) {
  res.render('index', { title: 'SCRIPT BROWSER TESTING' });
});

/* Try Async 
router.get('/test', function(req, res, next) {

  async.waterfall([
    function (cb) {
      
      console.log('fun1')
      fs.writeFileSync(path.join(__dirname, '../tests', '/test.js'), req.body.script, 'utf-8');
      cb();
    },
    function (cb) {
      console.log('fun2')
      exec(cmd, function(err, stdout, stderr) {
        console.log('execute run!')
        if (err !== null) {
          console.log('exec err: ' + err);
        } else {
         str = stdout;
         console.log(str);
         str = str.split('[0m').join(" ");
         str = str.split('[0;36m').join("");
         str = str.split('[0;35m').join("");
         str = str.split('[0;32m').join(" ");
       }
         
      cb(err, str);
      }); // end write exec
      
    }
  ], function (err, str) {
    console.log(finalResult);
    res.render('index', { title: 'SCRIPT BROWSER TESTING' });
       res.render('index', { 
        title: 'SCRIPT BROWSER TESTING',
        report: str 
      }); 
  });
});*/

router.post('/', function(req, res, next) {
  
  //write file
  fs.writeFileSync(path.join(__dirname, '../tests', '/test.js'), req.body.script, 'utf-8');
    
  exec(cmd, function(err, stdout, stderr) {
    console.log('execute run!');
    if (err !== null) {
      console.log('exec err: ' + err);
    } else {
     str = stdout;
     console.log(str);
     str = str.split('[0m').join(" ");
     str = str.split('[0;36m').join("");
     str = str.split('[0;35m').join("");
     str = str.split('[0;32m').join(" ");
    }
    
    res.render('index', { title: 'SCRIPT BROWSER TESTING', report: str });   

  }); // end exec

});// end router post

module.exports = router;
